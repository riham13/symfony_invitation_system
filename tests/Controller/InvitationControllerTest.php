<?php

// tests/Controller/InvitationControllerTest.php

namespace App\Tests\Controller;

use App\Entity\User;
use App\Entity\Invitation;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InvitationControllerTest extends WebTestCase
{
    public function testSendInvitation()
    {
        $client = static::createClient();
        $em = $client->getContainer()->get('doctrine')->getManager();
 
        $sender = new User();
        $sender->setName('name');
        $sender->setUsername('username');
        $sender->setPassword('password');
        $em->persist($sender);

        $invited = new User();
        $invited->setName('name');
        $invited->setUsername('invitedname');
        $invited->setPassword('password');
        $em->persist($invited);
        
        $em->flush();

        $data = [
            'sender_id' => $sender->getId(),
            'invited_id' => $invited->getId(),
        ];

        $client->request('POST', '/invitation/send', [], [], [], json_encode($data));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString('{"message" : "Invitation sent successfully!"}', $client->getResponse()->getContent());
    }

    public function testRespondInvitation()
    {
        $client = static::createClient();
        $em = $client->getContainer()->get('doctrine')->getManager();

        // Assuming existing invitation with id 7
        $invitationId = 7;

        $data = [
            'response' => 'accept',
        ];

        $client->request('PUT', "/invitation/respond/{$invitationId}", [], [], [], json_encode($data));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString('{"message" : "Invitation responded successfully!"}', $client->getResponse()->getContent());

        // Check if updated in the database
        $invitation = $em->getRepository(Invitation::class)->find($invitationId);
        $this->assertEquals('accepted', $invitation->getStatus());
    }

    public function testCancelInvitation()
    {
        $client = static::createClient();
        $em = $client->getContainer()->get('doctrine')->getManager();

        // Assuming existing invitation with iD 7
        $invitationId = 7;

        $client->request('DELETE', "/invitation/cancel/{$invitationId}");

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString('{"message" : "Invitation canceled successfully!"}', $client->getResponse()->getContent());

        // Check if deleted from the database
        $invitation = $em->getRepository(Invitation::class)->find($invitationId);
        $this->assertNull($invitation);
    }
}