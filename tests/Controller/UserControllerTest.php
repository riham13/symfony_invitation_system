<?php

// tests/Controller/UserControllerTest.php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testCreateUser()
    {
        $client = static::createClient();
 
        $data = [
            'name' => 'name',
            'username' => 'username',
            'password' => 'password',
        ];

        $client->request('POST', '/user/create', [], [], [], json_encode($data));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString('{"message" : "User Created Successfully!"}', $client->getResponse()->getContent());
    }
}