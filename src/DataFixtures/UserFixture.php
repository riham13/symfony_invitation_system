<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class UserFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        $user = new User();
        $user->setName('name');
        $user->setUsername('username');
        $user->setPassword('password');
        $manager->persist($user);
        
        $invited = new User();
        $invited->setName('invited_name');
        $invited->setUsername('invited_username');
        $invited->setPassword('invited_password');
        $manager->persist($invited);
        
        $manager->flush();

        $this->addReference('sender', $user);
        $this->addReference('invited', $invited);
    }
}
