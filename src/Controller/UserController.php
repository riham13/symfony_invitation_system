<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UserController extends AbstractController
{
    private $em;
    private $validator;
    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    #[Route('/user/create', name: 'create_user', methods: ['POST'])]
    public function createUser(Request $request): JsonResponse
    {
            $data = json_decode($request->getContent(), true);

            // Input Validation
            $constrains = new Assert\Collection([
                'name' => [new Assert\NotBlank()],
                'username' => [new Assert\NotBlank()],
                'password' => [new Assert\NotBlank()],
            ]);

            $violations = $this->validator->validate($data, $constrains);
            
            if(count($violations) > 0) {
                $errors = [];
                foreach ($violations as $violation) {
                    $errors[$violation->getPropertyPath()] = $violation->getMessage();
                }

                return new JsonResponse(['error' => $errors], 400);
            }

        try {
            $user = new User();
            $user->setName($data['name']);
            $user->setUsername($data['username']);
            $user->setPassword($data['password']);

            $this->em->persist($user);
            $this->em->flush();

            return new JsonResponse(['message' => 'User Created Successfully!'], 200);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }
    }
}
