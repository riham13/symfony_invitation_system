<?php

namespace App\Controller;

use App\Entity\Invitation;
use App\Entity\User;
use App\Repository\InvitationRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class InvitationController extends AbstractController
{
    private $em;
    private $userRepository;
    private $invitationRepository;
    public function __construct(EntityManagerInterface $em, UserRepository $userRepository, InvitationRepository $invitationRepository)
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->invitationRepository = $invitationRepository;
    }

    #[Route('/invitation/send', name: 'invitation_send', methods: ['POST'])]
    public function sendInvitation(Request $request): JsonResponse
    {
        try {
            $data = json_decode($request->getContent(), true);

            $senderId = $data['sender_id'] ?? null;
            $invitedId = $data['invited_id'] ?? null;

            if(!$senderId || !$invitedId || $senderId == $invitedId) {
                return new JsonResponse(['error' => 'Invalid Input!'], 400);
            }

            $sender = $this->userRepository->find($senderId);
            $invited = $this->userRepository->find($invitedId);

            if(!$sender || !$invited) {
                return new JsonResponse(['error' => 'User not found!'], 404);
            }

            $invitation = new Invitation();
            $invitation->setSender($sender);
            $invitation->setInvited($invited);
            $invitation->setStatus("sent");

            $this->em->persist($invitation);
            $this->em->flush();

            return new JsonResponse(['message' => 'Invitation sent successfully!'], 200);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }
    }

    #[Route('/invitation/cancel/{id}', name: 'invitation_cancel', methods: ['DELETE'])]
    public function cancelInvitation($id): JsonResponse
    {
        $invitation = $this->invitationRepository->find($id);

        if(!$invitation) {
            return new JsonResponse(['error' => 'Invitation not found!'], 404);
        }

        $this->em->remove($invitation);
        $this->em->flush();
        
        return new JsonResponse(['message' => 'Invitation canceled successfully!'], 200);

    }

    #[Route('/invitation/respond/{id}', name: 'invitation_respond', methods: ['PUT'])]
    public function respondInvitation($id, Request $request): JsonResponse
    {
        try {
            $data = json_decode($request->getContent(), true);

            $response = $data['response'] ?? null;

            if(!$response || !in_array($response, ['accept', 'decline'])) {
                return new JsonResponse(['error' => 'Invalid response!'], 400);
            }

            $invitation = $this->invitationRepository->find($id);
            
            if(!$invitation) {
                return new JsonResponse(['error' => 'Invitation not found!'], 404);
            }

            if($response == 'accept') {
                $invitation->setStatus('accepted');
            } else {
                $invitation->setStatus('declined');
            }

            $this->em->flush();

            return new JsonResponse(['message' => 'Invitation responded successfully!'], 200);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }
    }
}
